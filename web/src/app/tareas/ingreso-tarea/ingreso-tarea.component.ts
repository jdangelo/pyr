import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Tarea } from '../../services/lbsdk';

@Component({
  selector: 'app-ingreso-tarea',
  templateUrl: './ingreso-tarea.component.html',
  styleUrls: ['./ingreso-tarea.component.css']
})
export class IngresoTareaComponent implements OnInit {

  @ViewChild('inputTarea') miInput: ElementRef;
  @Output() onCrear: EventEmitter<Tarea> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  nuevaTarea(descripcion: string){
    if (descripcion.trim().length > 0) {
      let nuevaTarea : Tarea = new Tarea();
      nuevaTarea.descripcion = descripcion;
      this.onCrear.emit(nuevaTarea);
    } else {
      alert("carga la tarea!!!")
    }
    this.miInput.nativeElement.value = '';
  }

}
