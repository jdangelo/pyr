import { Component, OnInit } from '@angular/core';
import { TareaService } from '../../services/tarea.service';
import { HttpClient } from '@angular/common/http';
import { Tarea } from '../../services/lbsdk';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {

  tareas: any[] = [];
  filtroActual: any = {};
  constructor(
    private tareaService: TareaService,
    private http: HttpClient
  ) { }

  ngOnInit() {

    this.http.get('http://localhost:3000/api/Tareas')
    .subscribe((data: any[]) => {
      console.log('datos que llegan de la API: ', data);
      this.tareas = data;
    })    

  }

  recibirFiltro(filtro:any){
    this.filtroActual = filtro;
    this.actualizar();
  }

  actualizar() {
    this.tareaService.getAll(this.filtroActual)
      .subscribe(data => {
        console.log('tareas:', data)
        this.tareas = data;
      })
  }

  crearTarea(tarea: Tarea){
    this.tareaService.create(tarea)
    .subscribe((data) => {
      console.log('se creó la tarea ', data);
      this.actualizar();
    })
  }

  modificarTarea(tarea: Tarea){
    this.tareaService.modifyById(tarea)
    .subscribe((data) => {
      console.log('se modificó la tarea ', data);
      this.actualizar();
    })
  }

  borrarTarea(id: number){
    this.tareaService.deleteById(id)
    .subscribe((data) => {
      console.log('se eliminó la tarea ', data);
      this.actualizar();
    })
  }

}
