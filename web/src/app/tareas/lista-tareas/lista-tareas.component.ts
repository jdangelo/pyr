import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tarea } from '../../services/lbsdk';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {

  @Input() tareas:any[]
  @Output() onModificar: EventEmitter<Tarea> = new EventEmitter();
  @Output() onBorrar: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  modificar(tarea: Tarea){
    tarea.terminada = !tarea.terminada;
    this.onModificar.emit(tarea);
  }

  borrar(id: number){
    this.onBorrar.emit(id);
  }


}
