import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TareasComponent } from './tareas/tareas.component';
import { IngresoTareaComponent } from './ingreso-tarea/ingreso-tarea.component';
import { ListaTareasComponent } from './lista-tareas/lista-tareas.component';
import { FiltroTareasComponent } from './filtro-tareas/filtro-tareas.component';
import { TareasRoutingModule } from './tareas-routing.module';

@NgModule({
    declarations: [
        TareasComponent,
        IngresoTareaComponent,
        ListaTareasComponent,
        FiltroTareasComponent
    ],
    imports: [
        CommonModule,
        TareasRoutingModule
    ],
    exports: [],
    providers: [],
})
export class TareasModule { }