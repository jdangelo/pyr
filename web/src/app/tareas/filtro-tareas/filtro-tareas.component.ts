import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filtro-tareas',
  templateUrl: './filtro-tareas.component.html',
  styleUrls: ['./filtro-tareas.component.css']
})
export class FiltroTareasComponent implements OnInit {

  @Input() cantidadTareas: number;
  @Output() onFiltrar: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  
  filtrar(filtro: any){
    this.onFiltrar.emit(filtro);
  }

}
