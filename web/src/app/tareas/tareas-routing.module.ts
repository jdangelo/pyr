import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TareasComponent } from './tareas/tareas.component';


const routes: Routes = [
  {
    path: '',
    component: TareasComponent
  }, {
    path: 'todas',
    component: TareasComponent
  },
  {
    path: 'activas',
    component: TareasComponent
  },
  {
    path: 'terminadas',
    component: TareasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TareasRoutingModule { }