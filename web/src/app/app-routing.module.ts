
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TareasComponent } from './tareas/tareas/tareas.component';

const routes: Routes = [
    {
        path: 'tareas',
        //loadChildren: './tareas/tareas.module#TareasModule'
        component:TareasComponent
    },
    {
        path: 'pyr',
        loadChildren: './pyr/pyr.module#PyrModule'
    },
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
