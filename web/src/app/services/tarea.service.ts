import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { Tarea, TareaApi } from './lbsdk';
@Injectable({
  providedIn: 'root'
})
export class TareaService {

  constructor(private tareaAPI: TareaApi) { }

  getAll(filtro: any = {}): Observable<Tarea[]> {
    return this.tareaAPI.find({ where: filtro });
  }

  create(tarea: Tarea): Observable<Tarea> {
    return this.tareaAPI.create(tarea);
  }

  modifyById(tarea: Tarea): Observable<Tarea> {
    return this.tareaAPI.patchAttributes(tarea.id, tarea);
  }

  deleteById(id: number): Observable<Tarea> {
    return this.tareaAPI.deleteById(id);
  }



  /*
  getAll(): Observable<any[]> {
    //return Observable.create(this.getData)
    return Observable.create((observer: Observer<any[]>) => {
      setTimeout(()=>observer.next(this.tareas),1000)

    })
  }
  private getData(observer: Observer<any[]>) {
    observer.next(this.tareas)
  }*/
}
